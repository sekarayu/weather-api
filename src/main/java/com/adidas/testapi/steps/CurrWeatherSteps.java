package com.adidas.testapi.steps;

import com.adidas.testapi.constant.Constant;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.junit.Test;
import org.junit.Assert;

public class CurrWeatherSteps {

    static Constant constant;
    public static Response weatherByCity;
    public static Response weatherByCityCountry;
    public static Response weatherByCityId;
    public static Response weatherByLatLon;
    public static Response weatherByZipcode;

    @Step
    public void getWeatherByCity(String city){
        weatherByCity = SerenityRest.given().log().all().when().
                get(constant.BASE_URL+"weather?q="+city+"&appid="+constant.APP_ID);
        weatherByCity.then().assertThat().spec(new ResponseSpecBuilder().expectStatusCode(200).build());
    }

    public static String getWeather(Response response){
        String main;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json).setRoot("weather");
        main = jsonPath.get("main").toString().replaceAll("[\\W]","");
        return main;
    }

    public static String getCityName(Response response){
        String name;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json);
        name = jsonPath.get("name").toString().replaceAll("[\\W]","");
        return name;
    }

    @Step
    public void verifyWeatherByCity(String city){
        Assert.assertTrue(!getWeather(weatherByCity).isEmpty());
        Assert.assertTrue(getCityName(weatherByCity).toLowerCase().equals(city.toLowerCase()));
    }

    @Step
    public void getWeatherByCityCountry(String city, String country){
        weatherByCityCountry = SerenityRest.given().log().all().when().
                get(constant.BASE_URL+"weather?q="+city+","+country+"&appid="+constant.APP_ID);
        weatherByCityCountry.then().assertThat().spec(new ResponseSpecBuilder().expectStatusCode(200).build());
    }

    @Step
    public void verifyWeatherByCityCountry(String city, String country){
        Assert.assertTrue(!getWeather(weatherByCityCountry).isEmpty());
        Assert.assertTrue(getCityName(weatherByCityCountry).toLowerCase().equals(city.toLowerCase()));
        Assert.assertTrue(getCountry(weatherByCityCountry).toLowerCase().equals(country.toLowerCase()));
    }

    public static String getCountry(Response response){
        String main;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json).setRoot("sys");
        main = jsonPath.get("country").toString().replaceAll("[\\W]","");
        return main;
    }

    @Step
    public void getWeatherByCityId(String cityId){
        weatherByCityId = SerenityRest.given().log().all().when().
                get(constant.BASE_URL+"weather?id="+cityId+"&appid="+constant.APP_ID);
        weatherByCityId.then().assertThat().spec(new ResponseSpecBuilder().expectStatusCode(200).build());
    }

    @Step
    public void verifyWeatherByCityId(String city, String cityId){
        Assert.assertTrue(!getWeather(weatherByCityId).isEmpty());
        Assert.assertTrue("expected : "+getCityName(weatherByCityId).toLowerCase()+";actual : "+city.toLowerCase(),
                getCityName(weatherByCityId).toLowerCase().equals(city.toLowerCase()));
        Assert.assertTrue("expected : "+getCityId(weatherByCityId)+";actual : "+cityId,
                getCityId(weatherByCityId).equals(cityId));
    }

    public static String getCityId(Response response){
        String id;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json);
        id = jsonPath.get("id").toString();
        return id;
    }

    @Step
    public void getWeatherByLatLon(String lat, String lon){
        weatherByLatLon = SerenityRest.given().log().all().when().
                get(constant.BASE_URL+"weather?lat="+lat+"&lon="+lon+"&appid="+constant.APP_ID);
        weatherByLatLon.then().assertThat().spec(new ResponseSpecBuilder().expectStatusCode(200).build());
    }

    public static String getCoordLat(Response response){
        String lat;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json).setRoot("coord");
        lat = jsonPath.get("lat").toString();
        return lat;
    }

    public static String getCoordLon(Response response){
        String lon;
        String json = response.asString();
        JsonPath jsonPath = new JsonPath(json).setRoot("coord");
        lon = jsonPath.get("lon").toString();
        return lon;
    }

    @Step
    public void verifyWeatherByLatLon(String lat, String lon){
        Assert.assertTrue(!getWeather(weatherByLatLon).isEmpty());
        Assert.assertTrue("expected : "+getCoordLat(weatherByLatLon)+";actual : "+lat,
                getCoordLat(weatherByLatLon).equals(lat));
        Assert.assertTrue("expected : "+getCoordLon(weatherByLatLon)+";actual : "+lon,
                getCoordLon(weatherByLatLon).equals(lon));
    }

    @Step
    public void getWeatherByZipCode(String zipcode){
        weatherByZipcode = SerenityRest.given().log().all().when().
                get(constant.BASE_URL+"weather?zip="+zipcode+"&appid="+constant.APP_ID);
        weatherByZipcode.then().assertThat().spec(new ResponseSpecBuilder().expectStatusCode(200).build());
    }

    @Step
    public void verifyWeatherByZipCode(String defaultcountry){
        Assert.assertTrue(!getWeather(weatherByZipcode).isEmpty());
        Assert.assertTrue(getCountry(weatherByZipcode).toLowerCase().equals(defaultcountry));
    }

    @Test
    public void testgetweather(){
        getWeatherByCity("Paris");
        System.out.print("Weather : "+getWeather(weatherByCity));
    }

}
