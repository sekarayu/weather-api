Feature: Get Current Weather

  Scenario Outline: Get weather by city name
    Given the user wants to get current weather by city "<city>"
    Then current weather by city "<city>" is displayed

    Examples:
    |city|
    |singapore|

    Scenario Outline: Get weather by city name and country code
      Given the user wants to get current weather by city "<city>" and country "<country>"
      Then current weather by city "<city>" and country "<country>" is displayed

      Examples:
      |city|country|
      |singapore|sg|

  Scenario Outline: Get weather by city ID
    Given the user wants to get current weather by city id "<cityid>"
    Then current weather by city "<city>" and city id "<cityid>" is displayed

    Examples:
    |cityid|city|
    |6362983|zaragoza|

  Scenario Outline: Get weather by geo coordinate
    Given the user wants to get current weather by lat "<lat>" and lon "<lon>"
    Then current weather by lat "<lat>" and lon "<lon>" is displayed

    Examples:
    |lat|lon|
    |43|-75|

  Scenario Outline: Get weather by zip code
    Given the user wants to get current weather by zipcode "<zipcode>"
    Then current weather by zipcode "<countryid>" is displayed

    Examples:
    |zipcode|countryid|
    |94040  |us       |