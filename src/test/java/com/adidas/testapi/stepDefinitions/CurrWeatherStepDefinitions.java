package com.adidas.testapi.stepDefinitions;

import com.adidas.testapi.steps.CurrWeatherSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CurrWeatherStepDefinitions {

    @Steps
    CurrWeatherSteps currWeatherSteps;
    
    @Given("^the user wants to get current weather by city \"([^\"]*)\"$")
    public void theUserWantsToGetCurrentWeatherByCity(String arg0) throws Throwable {
        currWeatherSteps.getWeatherByCity(arg0);
    }

    @Then("^current weather by city \"([^\"]*)\" is displayed$")
    public void currentWeatherByCityIsDisplayed(String arg0) throws Throwable {
        currWeatherSteps.verifyWeatherByCity(arg0);
    }

    @Given("^the user wants to get current weather by city \"([^\"]*)\" and country \"([^\"]*)\"$")
    public void theUserWantsToGetCurrentWeatherByCityAndCountry(String arg0, String arg1) throws Throwable {
        currWeatherSteps.getWeatherByCityCountry(arg0,arg1);
    }

    @Then("^current weather by city \"([^\"]*)\" and country \"([^\"]*)\" is displayed$")
    public void currentWeatherByCityAndCountryIsDisplayed(String arg0, String arg1) throws Throwable {
        currWeatherSteps.verifyWeatherByCityCountry(arg0,arg1);
    }

    @Given("^the user wants to get current weather by city id \"([^\"]*)\"$")
    public void theUserWantsToGetCurrentWeatherByCityId(String arg0) throws Throwable {
        currWeatherSteps.getWeatherByCityId(arg0);
    }

    @Then("^current weather by city \"([^\"]*)\" and city id \"([^\"]*)\" is displayed$")
    public void currentWeatherByCityAndCityIdIsDisplayed(String arg0, String arg1) throws Throwable {
        currWeatherSteps.verifyWeatherByCityId(arg0,arg1);
    }

    @Given("^the user wants to get current weather by lat \"([^\"]*)\" and lon \"([^\"]*)\"$")
    public void theUserWantsToGetCurrentWeatherByLatAndLon(String arg0, String arg1) throws Throwable {
        currWeatherSteps.getWeatherByLatLon(arg0,arg1);
    }

    @Then("^current weather by lat \"([^\"]*)\" and lon \"([^\"]*)\" is displayed$")
    public void currentWeatherByLatAndLonIsDisplayed(String arg0, String arg1) throws Throwable {
        currWeatherSteps.verifyWeatherByLatLon(arg0,arg1);
    }

    @Given("^the user wants to get current weather by zipcode \"([^\"]*)\"$")
    public void theUserWantsToGetCurrentWeatherByZipcode(String arg0) throws Throwable {
        currWeatherSteps.getWeatherByZipCode(arg0);
    }

    @Then("^current weather by zipcode \"([^\"]*)\" is displayed$")
    public void currentWeatherByZipcodeIsDisplayed(String arg0) throws Throwable {
        currWeatherSteps.verifyWeatherByZipCode(arg0);
    }
}
